from django.db import models
from django.contrib.auth.models import User 
# Create your models here.
class feedback(models.Model):
    fname=models.CharField(max_length=125)
    lname=models.CharField(max_length=125)
    email=models.EmailField(max_length=200)
    phone=models.IntegerField()
    message=models.TextField(blank=True)
    added_on_DT=models.DateTimeField(auto_now_add=True) 
    def __str__(self):
        return self.fname
class course_type(models.Model):
    course_name=models.CharField(max_length=125)
    course_pic=models.ImageField(upload_to="media/course_img")
    description=models.TextField()
    added_on_DT=models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.course_name

class register(models.Model):
    user=models.OneToOneField(User,on_delete=models.CASCADE)
    phone=models.IntegerField()
    profile_pic=models.ImageField(upload_to="profile_pic/%Y/%m/%d",null=True,blank=True)
    age=models.CharField(max_length=250,null=True,blank=True)
    city=models.CharField(max_length=250,null=True,blank=True)
    education=models.TextField(null=True,blank=True)
    current_status=models.CharField(max_length=250,null=True,blank=True)
    gender=models.CharField(max_length=250,default="Male")
    added_on_DT=models.DateTimeField(auto_now_add=True,null=True) 
    updated_on=models.DateTimeField(auto_now=True,null=True) 
    def __str__(self):
        return self.user.username

class category(models.Model):
    name = models.CharField(max_length=250)
    cover_pic = models.FileField(upload_to="media/%y/%m/%d")
    description = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

class add_course(models.Model): 
    g=(
        ("F","Free"),("P","Paid")
    )
    teacher = models.ForeignKey(User,on_delete=models.CASCADE)
    course_title = models.CharField(max_length=250,blank=True)
    course_category = models.ForeignKey(category,on_delete = models.CASCADE)
    course_price = models.FloatField(blank=True)
    course_discount = models.FloatField(blank=True,null=True)
    cover_image = models.ImageField(upload_to="images/%y/%m/%d")
    total_hours = models.FloatField()
    description = models.TextField()
    course_curriculum = models.TextField(blank=True)
    course_type = models.CharField(max_length=250,blank=True,choices=g)
    course_video = models.FileField(upload_to="videos/%y/%m/%d",blank=True)
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.teacher.username

class cart(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    course = models.ForeignKey(add_course,on_delete = models.CASCADE)
    status = models.BooleanField(default=False)
    added_on = models.DateTimeField(auto_now_add=True,null=True)
    update_on = models.DateTimeField(auto_now_add=True,null=True)


    def __str__(self):
        return self.user.username

class Order(models.Model):
    stu_id = models.ForeignKey(User,on_delete=models.CASCADE)
    cart_ids = models.CharField(max_length=250)
    course_ids = models.CharField(max_length=250)
    invoice_id = models.CharField(max_length=250)  
    status = models.BooleanField(default=False)
    processed_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.stu_id.username       






















    

    
