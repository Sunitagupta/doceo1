from django import forms
from doceo_app.models import add_course


class add_course_form(forms.ModelForm):
    class Meta:
        model = add_course
        # fields = "__all__"
        fields = ["course_title","course_category","course_price","cover_image","course_type","description","course_curriculum","course_video","total_hours"]