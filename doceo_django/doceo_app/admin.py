from django.contrib import admin
from doceo_app.models import feedback,course_type,register,category,add_course,cart,Order
admin.site.site_header="DOCEO"
# Register your models here.
class feedbackAdmin(admin.ModelAdmin):
    list_display=["id","fname","lname","email","phone","added_on_DT"]
    search_fields=["fname"]
class course_typeAdmin(admin.ModelAdmin):
    list_display=["id","course_name","added_on_DT"]

admin.site.register(course_type,course_typeAdmin)    
admin.site.register(feedback,feedbackAdmin)
admin.site.register(register)
admin.site.register(category)
admin.site.register(add_course)
admin.site.register(cart)
admin.site.register(Order)