from django.shortcuts import render,get_object_or_404,reverse
from django.http import HttpResponse, HttpResponseRedirect,JsonResponse
from doceo_app.models import feedback,course_type,register,category,add_course,cart,Order
from django.contrib.auth.models import User 
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from doceo_app.forms import add_course_form
from django.db.models import Q
from datetime import datetime
from django.core.mail import EmailMessage
from paypal.standard.forms import PayPalPaymentsForm
from django.conf import settings

# Create your views here.
def home(request):
    if "user_id" in request.COOKIES:
        uid = request.COOKIES["user_id"]
        usr = get_object_or_404(User,id=uid)
        login(request,usr)
        if usr.is_superuser:
            return HttpResponseRedirect("/admin")
        if usr.is_active:
            return HttpResponseRedirect("/stu_dashboard")
    cour=course_type.objects.all()
    feed_data=feedback.objects.all()[:5]
    # print(feed_data)
    if request.method=="POST":
        fname=request.POST["fname"]
        lname=request.POST["lname"]
        email=request.POST["email"]
        phone=request.POST["contact"]
        username=request.POST["username"]
        password=request.POST["password"]
        utype=request.POST["utype"]
        #print(request.POST)
        user=User.objects.create_user(username,email,password)
        user.first_name=fname
        user.last_name=lname
        if utype=="tut":
            user.is_staff=True
        user.save()
        reg=register(user=user,phone=phone)
        reg.save()
        return render(request,"home.html",{"status":"{} {} your Account Created Successfully<br>Your Username is {}".format(fname,lname,username)})

        

    return render(request,"home.html",{"messages":feed_data,"course_t":cour})
def check_user(request):
    if request.method=="GET":
        un=request.GET["usern"]
        check =User.objects.filter(username=un)
        
        if len(check)==1:
            return HttpResponse("exists")
        else:
            return HttpResponse("not")

def user_login(request):
    if request.method=="POST":
        un=request.POST["username"]
        pwd=request.POST["password"]
        print(request.POST)

        user=authenticate(username=un, password=pwd)
        if user:
            login(request,user)
            if user.is_superuser:
                return HttpResponseRedirect("/admin")
            else:
                res = HttpResponseRedirect("/stu_dashboard")
                if "remme" in request.POST:
                    res.set_cookie("user_id",user.id)
                    res.set_cookie("date_login",datetime.now())
                return res
            # if user.is_active:
            #     return HttpResponseRedirect("/stu_dashboard")
        else:
            return render(request,"home.html",{"invalid_login":"Invalid Username or Password"})
    return HttpResponse("LOGIN CALLED")
@login_required
def user_logout(request):
    logout(request)
    res = HttpResponseRedirect("/")
    res.delete_cookie("user_id")
    res.delete_cookie("date_login")
    return res
def about(request):
    cour=course_type.objects.all()
    return render(request,"about.html",{"course_t":cour})

def contact(request):
    cour=course_type.objects.all()
    if request.method=="POST":
        fname=request.POST["fname"]
        lname=request.POST["lname"]
        email=request.POST["email"]
        phone=request.POST["phone"]
        message=request.POST["message"]
        data=feedback(fname=fname,lname=lname,email=email,phone=phone,message=message)
        data.save()
        res="Dear {} {} your Feedback Sumitted Succesfully".format(fname,lname)
        return render(request,"contact.html",{"status":res})
        return HttpResponse("data saved successfully")


    return render(request,"contact.html",{"course_t":cour})
def courses(request):
    cour=course_type.objects.all()
    # print(cour)
    return render(request,"courses.html",{"course_t":cour})
def courses_detail(request):
    context = {}
    all_courses = add_course.objects.all().order_by("course_title")
    context["courses"]= all_courses
    return render(request,"courses_detail.html",context)


@login_required
def stu_dashboard(request):
    context={}
    check = register.objects.filter(user_id=request.user.id)
    if len(check)>0:
        data = register.objects.get(user__id=request.user.id)
        context["data"]=data
    
    if request.method=="POST":
        print(request.POST)
        fname=request.POST["fname"]
        lname=request.POST["lname"]
        email=request.POST["email"]
        phone=request.POST["contact"]
        age=request.POST["age"]
        city=request.POST["city"]
        gen=request.POST["gen"]
        education=request.POST["education"]
        current_status=request.POST["current_status"]
        
        usr=User.objects.get(id=request.user.id)
        usr.first_name=fname
        usr.last_name=lname
        usr.email=email
        usr.save()


        data.phone=phone
        data.age=age
        data.city=city
        data.gender=gen
        data.education=education
        data.current_status=current_status
        data.save()
        if "profile_pic" in request.FILES:
            img=request.FILES["profile_pic"]
            data.profile_pic=img
            data.save()

        context["status"]="Thanks Your Profile Updated Succesfully"
    return render(request,"stu_dashboard.html",context)

@login_required
def tutor_dashboard(request):
    context={}
    data=register.objects.get(user__id=request.user.id)
    context["data"]=data
    if request.method=="POST":
        print(request.POST)
        fname=request.POST["fname"]
        lname=request.POST["lname"]
        email=request.POST["email"]
        phone=request.POST["contact"]
        age=request.POST["age"]
        city=request.POST["city"]
        gen=request.POST["gen"]
        education=request.POST["education"]
        current_status=request.POST["current_status"]
        
        usr=User.objects.get(id=request.user.id)
        usr.first_name=fname
        usr.last_name=lname
        usr.email=email
        usr.save()


        data.phone=phone
        data.age=age
        data.city=city
        data.gender=gen
        data.education=education
        data.current_status=current_status
        data.save()
        if "profile_pic" in request.FILES:
            img=request.FILES["profile_pic"]
            data.profile_pic=img
            data.save()

        context["status"]="Thanks Your Profile Updated Succesfully"
    return render(request,"tutor_dashboard.html",context)



def edit_profile(request):
    context = {}
    check = register.objects.filter(user_id=request.user.id)
    if len(check)>0:
        data = register.objects.get(user__id=request.user.id)
        context["data"]=data
    if request.method=="POST":
        fn = request.POST["firstname"]
        ln = request.POST["lastname"]
        em = request.POST["email"]
        con = request.POST["contact"]
        age = request.POST["age"]
        ct = request.POST["city"]
        gn = request.POST["gen"]
        lc = request.POST["education"]
        cs = request.POST["current_status"]

        usr=User.objects.get(id=request.user.id)
        usr.first_name=fn
        usr.last_name=ln
        usr.email=em
        usr.save()

        data.phone = con
        data.age = age
        data.city = ct
        data.gender = gn
        data.education = lc
        data.current_status = cs
        data.save()

        if "profile_pic" in request.FILES:
            img = request.FILES["profile_pic"]
            data.profile_pic = img
            data.save()
        context["status"] = "Thanks Your Profile Updated Succesfully!!!"
    return render(request,"edit_profile.html",context) 

def change_password(request):
    context = {}
    ch = register.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register.objects.get(user__id=request.user.id)
        context["data"] = data



    if request.method=="POST":
        current = request.POST["cpwd"]
        new_pas = request.POST["npwd"]

        user =User.objects.get(id=request.user.id)
        un = user.username
        check = user.check_password(current)
        if check==True:
            user.set_password(new_pas)
            user.save()
            context["msz"] = "Password Change Successfully!!!"
            context["col"] = "alert-success"
            user = User.objects.get(username=un)
            login(request,user)
        else:
            context["msz"] = "Incorrect Current Password"
            context["col"] = "alert-danger"
    return render(request,"change_password.html",context)     

def add_course_view(request):
    context = {}
    ch = register.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register.objects.get(user__id=request.user.id)
        context["data"] = data
    form = add_course_form()
    if request.method=="POST":
        form = add_course_form(request.POST,request.FILES)
        if form.is_valid():
            data = form.save(commit=False)
            login_user = User.objects.get(username=request.user.username)
            data.teacher = login_user
            data.save()
            context["status"] = "{} Added Successfully!!".format(data.course_title)
    
    context["form"] = form    
    return render(request,"addcourse.html",context)

def my_courses(request):
    context = {}
    ch = register.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register.objects.get(user__id=request.user.id)
        context["data"] = data
    all = add_course.objects.filter(teacher__id=request.user.id).order_by("-id")
    context["courses"] = all
    return render(request,"mycourses.html",context)   

def category_view(request):
    cats = category.objects.all()
    return render(request,"category.html",{"category":cats}) 

def video_view(request):
    context = {}
    if request.user.is_authenticated:
        id = request.GET["cid"]
        vid = add_course.objects.get(id=id)
        context["video"] = vid
    else:
        context["status"] = "Please Login First"    
    return render(request,"video.html",context)

def videos(request):
    context = {}
    ch = register.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register.objects.get(user__id=request.user.id)
        context["data"] = data
    id = request.GET["cid"]
    vid = add_course.objects.get(id=id)
    context["video"] = vid
    return render(request,"videos.html",context)           

def single_course(request):
    context = {}
    id = request.GET["cid"]
    obj = add_course.objects.get(id=id)
    context["course"] = obj
    return render(request,"singlecourse.html",context)
    

def update_course(request):
    context = {}
    ch = register.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register.objects.get(user__id=request.user.id)
        context["data"] = data
    cats = category.objects.all().order_by("name")
    context["category"] = cats

    cid = request.GET["cid"]
    course =  get_object_or_404(add_course,id=cid)
    context["course"] = course

    if request.method=="POST":
        cn = request.POST["pname"]
        ct_id = request.POST["cat"]
        cp = request.POST["cp"]
        des = request.POST["des"]
        
        cat_obj = category.objects.get(id=ct_id) 

        course.course_title = cn
        course.course_category = cat_obj
        course.course_price = cp
        course.description = des
        if "cimg" in request.FILES:
            img = request.FILES["cimg"]
            course.cover_image = img

        if "video" in request.FILES:
            video = request.FILES["video"]
            course.course_video = video
        course.save() 
        context["status"] = "Changes saved Successfully"   
        context["id"] = cid  


    return render(request,"update_courses.html",context)

def delete_course(request):
    context = {}
    ch = register.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register.objects.get(user__id=request.user.id)
        context["data"] = data
    if "cid" in request.GET:
        cid = request.GET["cid"]
        cour = get_object_or_404(add_course,id=cid)
        context["course"] = cour

        if "action" in request.GET:
            cour.delete()
            context["status"] = str(cour.course_title)+"Removed Successfully!!!"
    return render(request,"Deletecourse.html",context)

def all_courses(request):
    context = {}
    all_courses = add_course.objects.all().order_by("course_title")
    context["courses"]= all_courses
    if "qry" in request.GET:
        q=request.GET["qry"]
        cour = add_course.objects.filter(Q(course_title__contains=q)|Q(course_category__name__contains=q))
        context["courses"] = cour
        context["abcd"] = "search"
    if "cat" in request.GET:
        cid = request.GET["cat"]
        cour = add_course.objects.filter(course_category__id=cid)
        context["courses"] = cour
        context["abcd"] = "search"    


    return render(request,"allcourses.html",context)

def sendemail(request):
    context = {}
    ch = register.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register.objects.get(user__id=request.user.id)
        context["data"] = data 
    if request.method == "POST":
        rec = request.POST["to"].split(",")
        sub = request.POST["sub"]
        msz = request.POST["msz"]
        try:
            em = EmailMessage(sub,msz,to=rec)
            em.send()
            context["message"]  = "Email sent Succesfully" 
            context["cls"]  = "alert-success" 
        except:
            context["message"]  = "Could not send,Please check Internet Connection/Email Adress" 
            context["cls"]  = "alert-danger"       
    return render(request,"sendemail.html",context)


def forgotpass(request):
    context ={}
    if request.method=="POST":
        un=request.POST["username"]
        pwd=request.POST["npass"]

        user = get_object_or_404(User,username=un)
        user.set_password(pwd)
        user.save()

        login(request,user)
        if user.is_superuser:
            return HttpResponseRedirect("/admin")
        else:
            return HttpResponseRedirect("/stu_dashboard")    
        # context["status"] = "Password Changed Successfully!!! "
    return render(request,"forgotpass.html",context) 

import random

def reset_password(request):
    un = request.GET["username"]
    try: 
        user = get_object_or_404(User,username=un) 
        otp = random.randint(1000,9999)
        msz =  "Dear {}\n{} is your One Time Password(OTP) \nDo not share it with others \nThanks&Regards \n Doceo ".format(user.username,otp)
        try:
            email = EmailMessage("Account Verfication",msz,to=[user.email])
            email.send()
            return JsonResponse({"status":"sent","email":user.email,"rotp":otp}) 
        except:
            return JsonResponse({"status":"error","email":user.email})      
    except:
        return JsonResponse({"status":"failed"})

def add_to_cart(request):
    context={}
    items = cart.objects.filter(user__id=request.user.id,status=False)
    context["items"] = items
    if request.user.is_authenticated:
        if request.method=="POST":
            cid = request.POST["cid"]
            is_exist = cart.objects.filter(course__id=cid,user__id=request.user.id,status=False)
            if len(is_exist)>0:
                context["msz"] = "Item Already Exists in Your Cart"
                context["cls"] = "alert alert-warning"
            else:
                course = get_object_or_404(add_course,id=cid)
                usr = get_object_or_404(User,id=request.user.id)
                c  = cart(user=usr,course=course)
                c.save()
                context["msz"] = "{} Added in Your Cart".format(course.course_title)
                context["cls"] = "alert alert-success"
    else:
        context["status"] = "Please  Login To Veiw Your Cart"
    return render(request,"cart.html",context)        

def get_cart_data(request):
    items = cart.objects.filter(user__id=request.user.id,status=False)
    total=0
    for i in items:
        total += float(i.course.course_price)

    res = {
        "total":total,
    }    
    return JsonResponse(res)

def change_quan(request):
    if "delete_cart" in request.GET:
        id = request.GET["delete_cart"]
        cart_obj = get_object_or_404(cart,id=id)
        cart_obj.delete()
        return HttpResponse(1)

def process_payment(request):
    items = cart.objects.filter(user_id__id=request.user.id,status=False)
    courses = ""
    amt = 0
    inv = "INV10001-"
    cart_ids = ""
    c_ids = ""
    for j in items:
        courses = str(j.course.course_title)+"\n"
        c_ids = str(j.course.id)+","
        amt += float(j.course.course_price)
        inv += str(j.id)
        cart_ids += str(j.id)+","
    paypal_dict = {
        'business': settings.PAYPAL_RECEIVER_EMAIL,
        'amount': str(amt),
        'item_name': courses,
        'invoice': inv,
        'notify_url': 'http://{}{}'.format('127.0.0.1:8000',
                                           reverse('paypal-ipn')),
        'return_url': 'http://{}{}'.format('127.0.0.1:8000',
                                           reverse('payment_done')),
        'cancel_return': 'http://{}{}'.format('127.0.0.1:8000',
                                              reverse('payment_cancelled')),                                   
    }

    usr = User.objects.get(username=request.user.username)
    ord =Order(stu_id=usr,cart_ids=cart_ids,course_ids=c_ids)
    ord.save()
    ord.invoice_id = str(ord.id)+inv
    ord.save()
    request.session["order_id"] = ord.id

    form = PayPalPaymentsForm(initial=paypal_dict)
    return render(request, 'process_payment.html', { 'form': form})


def payment_done(request):
    if "order_id" in request.session:
        order_id = request.session["order_id"]
        ord_obj = get_object_or_404(Order,id=order_id)
        ord_obj.status=True
        ord_obj.save()
        
        for i in ord_obj.cart_ids.split(",")[:-1]:
            cart_object  = cart.objects.get(id=i)
            cart_object.status=True
            cart_object.save()
    return render(request,"paymentsuccess.html")               

def payment_cancelled(request):
    return render(request,"paymentfailed.html")

def order_history(request):
    context = {}
    ch = register.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register.objects.get(user__id=request.user.id)
        context["data"] = data
    all_orders = []
    orders = Order.objects.filter(stu_id__id=request.user.id).order_by("-id") 
    for order in orders:
        courses = []
        for id in order.course_ids.split(",")[:-1]:
            cour = get_object_or_404(add_course,id=id) 
            courses.append(cour)
        ord = {
            "order_id":order.id,
            "courses":courses,
            "invoice":order.invoice_id,
            "status":order.status,
            "date":order.processed_on,
        }      
        all_orders.append(ord)
    context["order_history"] = all_orders    
    return render(request,"order_history.html",context)

def my_stu(request):
    context = {}
    ch = register.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register.objects.get(user__id=request.user.id)
        context["data"] = data

    courses = cart.objects.filter(course__teacher__id=request.user.id,status=True)
    stu = []
    ids = []
    for i in courses:
        us ={
            "username":i.user.username,
            "first_name":i.user.first_name,
            "last_name":i.user.last_name,
            "email":i.user.email,
            "join":i.user.date_joined,
        }
        check = register.objects.filter(user__id=i.user.id)
        if len(check)>0:
            prf = get_object_or_404(register,user__id=i.user.id)
            us["profile_pic"]=prf.profile_pic
            us["contact"]=prf.phone
        ids.append(i.user.id)
        count = ids.count(i.user.id)

        if count<2:
            stu.append(us)

    context["student"] = stu        

    return render(request,"mystu.html",context)                          