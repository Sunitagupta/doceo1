"""doceo_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from doceo_app import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.home,name="home"),
    path('about/',views.about,name="about"),
    
    path('contact/',views.contact,name="contact"),
    path('courses/',views.courses,name="courses"),
    
    path('courses_detail/',views.courses_detail,name="courses_detail"),
    path('stu_dashboard/',views.stu_dashboard,name="stu_dashboard"),
    path('tutor_dashboard/',views.tutor_dashboard,name="tutor_dashboard"),
    path('check_user/',views.check_user,name="check_user"),
    path('user_login/',views.user_login,name="user_login"),
    path('user_logout/',views.user_logout,name="user_logout"),
    path('change_password/',views.change_password,name="change_password"),
    path('edit_profile/',views.edit_profile,name="edit_profile"),
    path('add_course_view/',views.add_course_view,name="add_course_view"),
    path('my_courses/',views.my_courses,name="my_courses"),
    path('category_view/',views.category_view,name="category_view"),
    path('single_course/',views.single_course,name="single_course"),
    path('update_course/',views.update_course,name="update_course"),
    path('delete_course/',views.delete_course,name="delete_course"),
    path('all_courses/',views.all_courses,name="all_courses"),
    path('sendemail/',views.sendemail,name="sendemail"),
    path("forgotpass/",views.forgotpass,name="forgotpass"),   
    path("reset_password/",views.reset_password,name="reset_password"),
    path("video_view/",views.video_view,name="video_view"),
    path("add_to_cart/",views.add_to_cart,name="add_to_cart"),
    path("videos/",views.videos,name="videos"),
    path("get_cart_data/",views.get_cart_data,name="get_cart_data"),
    path("change_quan/",views.change_quan,name="change_quan"),
    path("process_payment/",views.process_payment,name="process_payment"),
    path("payment_done/",views.payment_done,name="payment_done"),
    path("payment_cancelled/",views.payment_cancelled,name="payment_cancelled"),
    path("order_history/",views.order_history,name="order_history"),
    path("my_stu/",views.my_stu,name="my_stu"),

    path('paypal/', include('paypal.standard.ipn.urls')),

]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
